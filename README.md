# loggy

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6643881e603c4228be5d09ea3fdfb220)](https://www.codacy.com/app/zlien/loggy?utm_source=yubb@bitbucket.org&amp;utm_medium=referral&amp;utm_content=yubb/loggy&amp;utm_campaign=Badge_Grade)

Loggy is a simple, lightweight logger for PHP, designed to log to stdout or files in JSON format. 

The impetus for this package is to have a consistent log format across all applications in the web stack, with JSON output as to be easily consumed by a log forwarder and pushed to an ELK stack with minimal processing.

Loggy can be used via logger objects or using static methods for ease of use in your application.

Static methods uses the Singleton pattern to prevent redundancy.

# Installation
Loggy can be installed in your app via [composer](https://getcomposer.org)

```
#!bash
composer require yubb/loggy
```


# Usage

- Loggy implements `Psr\Log\LoggerInterface`.
- Loggy can be configured to write logs to stdout (default behavior) or write to files.
- If writing to a file, loggy can rotate log files on Daily, Weekly or Monthly basis.
- Loggy have different formats for logs, JSON (default), Pretty JSON, or Line.
    - Json: logs formatted as a JSON object.
    - Pretty JSON: logs formatted as a JSON object with timestamp converted to human-friendly format (ISO-8601 which is still ELK stack compatible).
    - Line: logs formatted as a line. (more suitable for dev environments.)

## Log Structure

|   Field	|   Type	|   Note	|
|---	|---	|---	|
|  timestamp 	|   `UNIX seconds epoch`	|   UNIX seconds epoch that is timezone independent	|
|   channel	|   `String`	|   logger's channel	|
|   level	|  `String` 	|   	|
|   level_value	|   `Integer`	|   	|
|   message	|   `String`	|   	|
|   context	|   `JSON object`	|   	|
|   backtrace	|   `JSON object`	|   This contains the `line`, `function`, `class`, and `file` at where the log was written. 	|

## Configuration
This is configuration parameters for logger's constructor or static Loggy's configuration.

|   Argument	|   Type	|   Note	|  Default | 
|---	|---	|---	|---	|
|  $channel 	|   `String`	|   Logger's channel	|
|  $min_log_level	|   `Integer` OR `Loggy's level consts`	|   Minimum level at which log will be written	| `Loggy::DEBUG`
|  $format	|   `Integer` OR `Loggy's format consts`	|   The format of logs	| `Loggy::JSON`
|  $stream	|   `String` OR `Loggy's stdout/stderr consts`	|   The stream at which logs will be written, can be either a filepath or stdout/stderr	| `Loggy::STDOUT`
|  $rotation	|   `Date Format` OR `Loggy's rotation consts`	|  Will add a date formatted by the given format ( Only relevant if $stream is a filepath )	| `Loggy::FILE_NO_ROTATE`

Other Logging rotation consts are:

```
    Loggy::FILE_NO_ROTATE = ''    
    Loggy::FILE_PER_DAY = 'Y-m-d'
    Loggy::FILE_PER_WEEK = 'Y-W'
    Loggy::FILE_PER_MONTH = 'Y-m'
```

## Basic usage (Using static methods)

At the entrypoint of your application, you should set loggy.

```
#!php
<?php
Loggy::setLogger('my-app');
```

From there, any calls in the request will use this loggy instance ONLY and will display logs with the given channel.

```
#!php
<?php
Loggy::info_s('TEST DEBUG', ['foo' => 'bar', 'fizz' => 'buzz']);
```

This will produce a log to stdout that looks like:
```
#!json
{
  "timestamp": "1549908364",
  "channel": "my-app",
  "level": "DEBUG",
  "level_value": 100,
  "message": "TEST DEBUG",
  "context": {
    "foo": "bar",
    "fizz": "buzz"
  },
  "backtrace": {
      "line": 30,
      "function": "DoTheThing",
      "class": "MyApp\\MyAppClass",
      "file": "/myappdirectory/MyAppClass.php"
  }
}
```

(Note: the log will be only one line, and not formatted as nicely as above)

### Min log level

When setting loggy, you can define the minimum log level as such

```
#!php
<?php
Loggy::setLogger('my-app', Loggy::INFO);
```

### Pretty format

When setting loggy, you can set it to log in 'pretty' format, which will print the logs in a more human format instead of JSON. This is usually good for dev environments.

#### PRETTY_JSON
```
#!php
<?php
Loggy::setLogger('my-app', Loggy::INFO, Loggy::PRETTY_JSON);
```
Pretty Json is the same as `JSON` except that it converts timestamp to human readable date instead of unix epoch.


#### LINE
```
#!php
<?php
Loggy::setLogger('my-app', Loggy::INFO, Loggy::LINE);
```
Logs will be sent to stdout and look like:
```
2019-01-23 14:06:12.816700 [INFO] <my-app> TEST INFO {"foo":"bar","fizz":"buzz"} (in /myappdirectory/MyAppClass.php, MyApp\\MyAppClass:DoTheThing, line 30)
```