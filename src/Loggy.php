<?php

/**
 * Class Loggy
 */
class Loggy implements Psr\Log\LoggerInterface
{
    // RFC 5424 Log levels

    /**
     * Debug information
     */
    const DEBUG = 100;

    /**
     * Info events
     *
     * Examples: User logs in, SQL logs.
     */
    const INFO = 200;

    /**
     * For normal but significant condition
     */
    const NOTICE = 250;

    /**
     * Exceptional occurrences that are undesired. (not errors and are not necessarily wrong).
     */
    const WARNING = 300;

    /**
     * Error conditions
     */
    const ERROR = 400;

    /**
     * Critical conditions
     */
    const CRITICAL = 500;

    /**
     * Action must be taken immediately
     */
    const ALERT = 550;

    /**
     * System is unusable.
     */
    const EMERGENCY = 600;

    /**
     * @var array RFC 5424 Log levels
     */
    protected static $levels = array(
        self::DEBUG     => 'DEBUG',
        self::INFO      => 'INFO',
        self::NOTICE    => 'NOTICE',
        self::WARNING   => 'WARNING',
        self::ERROR     => 'ERROR',
        self::CRITICAL  => 'CRITICAL',
        self::ALERT     => 'ALERT',
        self::EMERGENCY => 'EMERGENCY',
    );

    /**
     * JSON formatting - more suitable format for machines.
     */
    const JSON = 1;

    /**
     * JSON formatting with timestamp as date instead of epoch.
     */
    const PRETTY_JSON = 2;

    /**
     * LINE formatting - more readable format for humans.
     */
    const LINE = 3;

    /**
     * @var array Different supported formatting(s).
     */
    protected static $formats = array(
        self::JSON => 'JSON',
        self::PRETTY_JSON => 'PRETTY_JSON',
        self::LINE => 'LINE',
    );

    /**
     * standard output stream.
     */
    const STDOUT = 'php://stdout';

    /**
     * standard error stream.
     */
    const STDERR = 'php://stderr';

    /**
     * Rotate files per day.
     */
    const FILE_NO_ROTATE = '';

    /**
     * Rotate files per day.
     */
    const FILE_PER_DAY = 'Y-m-d';

    /**
     * Rotate files per week.
     */
    const FILE_PER_WEEK = 'Y-W';

    /**
     * Rotate files per month.
     */
    const FILE_PER_MONTH = 'Y-m';

    /**
     * @var array Different supported formatting(s).
     */
    protected static $rotation_strategies = array(
        self::FILE_NO_ROTATE => '',
        self::FILE_PER_DAY => 'Y-m-d',
        self::FILE_PER_WEEK => 'Y-W',
        self::FILE_PER_MONTH => 'Y-m',
    );

    /**
     * @var Loggy static instance.
     */
    protected static $instance;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var int
     */
    protected $min_log_level;

    /**
     * @var int
     */
    protected $format;

    /**
     * @var string
     */
    protected $stream;

    /**
     * @var array
     */
    protected $default_context;

    /**
     * @param string $channel a descriptive name that is attached to all log using this logger.
     * @param int $min_log_level minimum level at which logs will be written
     * @param int $format format of the log.
     * @param string $stream stream which logs will be written, it's either a filepath or a stream such as stdout or stderr.
     * @param string $rotation date format that will be postfix-ed to log files (only relevant if $stream is a filepath.)
     */
    public function __construct($channel, $min_log_level = self::DEBUG, $format = self::JSON, $stream = self::STDOUT, $rotation = self::FILE_NO_ROTATE)
    {
        // checking minimum logging level
        if (!isset(static::$levels[$min_log_level])) {
            throw new Psr\Log\InvalidArgumentException("Minimum Level: $min_log_level is undefined. please use one of: " . implode(', ', array_keys(self::$levels)));
        }

        // check log levels
        if (!isset(self::$formats[$format])) {
            throw new Psr\Log\InvalidArgumentException("format: $format  is undefined. please use one of: " . implode(', ', array_keys(self::$formats)));
        }

        // check rotation strategy
        if (!isset(self::$rotation_strategies[$rotation])) {
            throw new Psr\Log\InvalidArgumentException("rotation strategy: $rotation  is undefined. please use one of: " . implode(', ', array_keys(self::$rotation_strategies)));
        }

        // Add rotation prefix if rotation enabled & stream is not stdout or stderr
        if ($rotation !== self::FILE_NO_ROTATE && $stream !== self::STDOUT && $stream != self::STDERR) {
            $stream = self::getTimedFilename($stream, $rotation);
        }

        $this->channel = $channel;
        $this->min_log_level = $min_log_level;
        $this->format = $format;
        $this->stream = $stream;
        $this->default_context = [];
    }

    /**
     * @inheritdoc
     */
    public function debug($message, array $context = array())
    {
        $this->log(self::DEBUG, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function info($message, array $context = array())
    {
        $this->log(self::INFO, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function notice($message, array $context = array())
    {
        $this->log(self::NOTICE, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function warning($message, array $context = array())
    {
        $this->log(self::WARNING, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function error($message, array $context = array())
    {
        $this->log(self::ERROR, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function critical($message, array $context = array())
    {
        $this->log(self::CRITICAL, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function alert($message, array $context = array())
    {
        $this->log(self::ALERT, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function emergency($message, array $context = array())
    {
        $this->log(self::EMERGENCY, $message, $context);
    }

    /**
     * @inheritdoc
     */
    public function log($log_level, $message, array $context = array())
    {
        // Logging to undefined level.
        if (!isset(static::$levels[$log_level])) {

            // check if the string value of $log_level corresponds to a defined constant.
            if (!defined(__CLASS__.'::'.strtoupper($log_level))) {
                throw new Psr\Log\InvalidArgumentException("Level: $log_level is undefined. please use one of: " . implode(', ', array_keys(self::$levels)));
            }

            // get actual level integer value If the string value of $log_level corresponds to a defined constant.
            $log_level = constant(__CLASS__.'::'.strtoupper($log_level));
        }

        // Check minimum logging level.
        if ($log_level < $this->min_log_level)
        {
            return;
        }

        $log = self::formatLog(
            $this->channel,
            $log_level,
            $message,
            array_merge($this->default_context, $context),
            self::getBacktrace(),
            $this->format
        );

        // Open stream
        $stream = fopen($this->stream, 'a');

        // Write log record (no need to flock() file, since 'append' mode is atomic aka thread-safe.)
        fwrite($stream, $log . PHP_EOL);

        // Close stream.
        fclose($stream);
    }

    /**
     * extend logger by adding a new path segment to the logger's channel name.
     * Segments are joined by periods.
     * @param $sub_channel
     * @return Loggy
     */
    public function extend($sub_channel){
        // If string is null, empty or a space return same instance.
        if(!isset($sub_channel) || trim($sub_channel) === ''){
            return $this;
        }

        // Clone logger with extended channel name.
        return new Loggy(
            $this->channel . '.' . $sub_channel,
            $this->min_log_level,
            $this->format,
            $this->stream
        );
    }

    /**
     * Set up static Loggy's logger properties. Call once at the entry point of the request.
     * Once the static instance and it's properties are set, they cannot be changed (and we'll print warnings if changes are attempted).
     * @param string $channel Name of the 'app-space' stream for these logs
     * @param int $min_log_level
     * @param int $format
     * @param string $stream
     * @param string $rotation
     */
    public static function setLogger($channel, $min_log_level = self::DEBUG, $format = self::JSON, $stream = self::STDOUT, $rotation = self::FILE_NO_ROTATE)
    {
        if(self::$instance == null)
        {
            self::$instance = new Loggy($channel, $min_log_level, $format, $stream, $rotation);
        }
        else
        {
            self::$instance->log(self::WARNING, "Loggy instance already set!", func_get_args());
        }
    }

    /**
     * Return the static instance of Loggy  set by ::SetLogger(), typically to extend it.
     * @return Loggy
     */
    public static function getLogger()
    {
        return self::$instance;
    }

    /**
     * Set the logger's default context, to be included with the context for every log.
     */
    public function setDefaultContext(array $context)
    {
        $this->default_context = $context;
    }

    /**
     * Log a message at level DEBUG
     * @param string $message
     * @param array $context
     */
    public static function add_debug($message, array $context = array())
    {
        static::$instance->log(self::DEBUG, $message, $context);
    }

    /**
     * Log a message at level INFO
     * @param string $message
     * @param array $context
     */
    public static function add_info($message, array $context = array())
    {
        static::$instance->log(self::INFO, $message, $context);
    }

    /**
     * Log a message at level NOTICE
     * @param string $message
     * @param array $context
     */
    public static function add_notice($message, array $context = array())
    {
        static::$instance->log(self::NOTICE, $message, $context);
    }

    /**
     * Log a message at level WARNING
     * @param string $message
     * @param array $context
     */
    public static function add_warning($message, array $context = array())
    {
        static::$instance->log(self::WARNING, $message, $context);
    }

    /**
     * Log a message at level ERROR
     * @param string $message
     * @param array $context
     */
    public static function add_error($message, array $context = array())
    {
        static::$instance->log(self::ERROR, $message, $context);
    }

    /**
     * Log a message at level CRITICAL
     * @param string $message
     * @param array $context
     */
    public static function add_critical($message, array $context = array())
    {
        static::$instance->log(self::CRITICAL, $message, $context);
    }

    /**
     * Log a message at level ALERT
     * @param string $message
     * @param array $context
     */
    public static function add_alert($message, array $context = array())
    {
        static::$instance->log(self::ALERT, $message, $context);
    }

    /**
     * Log a message at level EMERGENCY
     * @param string $message
     * @param array $context
     */
    public static function add_emergency($message, array $context = array())
    {
        static::$instance->log(self::EMERGENCY, $message, $context);
    }

    /**
     * Formats and returns the text of the log to be written.
     * Format is based on value of $format.
     * @param string $channel
     * @param int $log_level
     * @param string $message
     * @param array $context
     * @param array $backtrace
     * @param int $format
     * @return string log text
     */
    private static function formatLog($channel, $log_level, $message, $context, $backtrace, $format)
    {
        $log = null;
        $log_dict = [
            'timestamp' => (string)time(),
            'channel' => $channel,
            'level' => self::$levels[$log_level],
            'level_value' => $log_level,
            'message' => $message,
            'context' => $context,
            'backtrace' => $backtrace,
            'pid' => getmypid(),
        ];

        switch ($format) {
            case self::JSON:
                // Json string format.
                $log = json_encode($log_dict, JSON_UNESCAPED_SLASHES | JSON_PARTIAL_OUTPUT_ON_ERROR);
                break;
            case self::PRETTY_JSON:
                // Get a readable Datetime format
                $log_dict['timestamp'] = self::getTimestamp($log_dict['timestamp']);

                // Json string format.
                $log = json_encode($log_dict, JSON_UNESCAPED_SLASHES | JSON_PARTIAL_OUTPUT_ON_ERROR);
                break;
            case self::LINE:
                // Get a readable Datetime format
                $log_dict['timestamp'] = self::getTimestamp($log_dict['timestamp']);

                // 'Pretty' format
                $log = sprintf("%s [%s] <%s> %s %s (in %s, %s:%s, line %d) (pid: %d)",
                    $log_dict['timestamp'],
                    $log_dict['level'],
                    $log_dict['channel'],
                    $log_dict['message'],
                    json_encode($log_dict['context']),
                    $log_dict['backtrace']['file'],
                    $log_dict['backtrace']['class'],
                    $log_dict['backtrace']['function'],
                    $log_dict['backtrace']['line'],
                    $log_dict['pid']
                );
                break;
        }

        return $log;
    }

    /**
     * Convert Unix Epoch to UTC datetime formatted using DATE_ATOM format (aka ISO-8601).
     * @param $epoch
     * @return string Timestamp at call time
     */
    private static function getTimestamp($epoch)
    {
        $timestamp = DateTime::createFromFormat("U", $epoch, new DateTimeZone('UTC'));
        return $timestamp->format(DateTime::ATOM);
    }

    /**
     * Returns a subset of debug_backtrace (file, line, class, function).
     * This function is tuned to work at a very specific spot in the call stack (e.g. this <- $this->writeLog() <- Loggy::debug()).
     * As a result of the main Loggy methods being called in static context only, we'll need 4 items back in the stack.
     * e.g. :
     *   0th is this
     *   1st is writeLog()
     *   2nd is Loggy::debug()
     *   3rd is myApplicationFunction()
     *
     * @return array Dictionary with key set {file, line, class, function}
     */
    private static function getBacktrace()
    {
        //TODO costs a lot of resources, may need to make it only to >= ERROR logs only.

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);

        // logged via {method} -> log().
        if ($backtrace[2]['class'] === __CLASS__) {
            $file = isset($backtrace[2]['file']) ? $backtrace[2]['file'] : '';
            $line = isset($backtrace[2]['line']) ? $backtrace[2]['line'] : '';
            $class = isset($backtrace[3]['class']) ? $backtrace[3]['class'] : '';
            $function = isset($backtrace[3]['function']) ? $backtrace[3]['function'] : '';
        } else // logged via ->log() directly.
        {
            $file = isset($backtrace[1]['file']) ? $backtrace[1]['file'] : '';
            $line = isset($backtrace[1]['line']) ? $backtrace[1]['line'] : '';
            $class = isset($backtrace[2]['class']) ? $backtrace[2]['class'] : '';
            $function = isset($backtrace[2]['function']) ? $backtrace[2]['function'] : '';
        }

        return [
            'file' => $file,
            'line' => $line,
            'class' => $class,
            'function' => $function
        ];
    }

    /**
     * @param string $filepath
     * @param string $rotation_format rotation date format
     * @return string path to file with rotation time prefix added.
     */
    public static function getTimedFilename($filepath, $rotation_format)
    {
        $fileInfo = pathinfo($filepath);

        $timedFilename = $fileInfo['dirname'] . '/' . $fileInfo['filename'] . '-' . date($rotation_format);

        if (!empty($fileInfo['extension'])) {
            $timedFilename .= '.' . $fileInfo['extension'];
        }

        return $timedFilename;
    }
}